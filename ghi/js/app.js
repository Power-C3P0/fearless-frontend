function createCard(name, description, pictureUrl, startDate, endDate, locName) {
    return `
    <div class="shadow mb-5 bg-body rounded">
      <div class="card">
        <img src=${pictureUrl} class="card-img-top" alt"...">
        <div class="card-header">
            <h5 class="card-title">${name}</h5>
            <p class="card-subtitle text-secondary"">${locName}</p>
        </div>
        <div class="card-body">
          <p class="card-text">${description}</p>
        </div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item">${startDate} - ${endDate}</li>
        </ul>
      </div>
    </div>
    `;
  }

function showNav(){
    return `
        <li class="nav-item">
                <a class="nav-link active" aria-current="page" href="#">Home</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="new-location.html">New location</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="new-conference.html">New conference</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="new-presentation.html">New presentation</a>
        </li>
    `
}

function createAlert(error){
    return `
        <div class="alert alert-warning" role="alert">
            <h2>${error}</h2>
            <p>shits' fucked</p>
        </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
    try{
        const response = await fetch(url);
        if(!response.ok){
            console.log("response no good");
        } else {
            const data = await response.json();
             document.getElementById('ifNav').innerHTML = showNav();
            let colNum = 1
            for(let conference of data.conferences){
                // console.log(conference);
                // console.log(data)
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if(detailResponse.ok){
                    const details = await detailResponse.json();
                    const con = details.conference;
                    const name = con.name;
                    const img = con.location.picture_url;
                    const description = con.description;
                    const startDate = new Date(con.starts).toLocaleDateString();
                    const endDate = new Date(con.ends).toLocaleDateString();
                    const locName = con.location.name
                    const html = createCard(name, description, img, startDate, endDate, locName);
                    let column = document.querySelector(`#colNum${colNum}`);
                    column.innerHTML = html
                    colNum++
                    if(colNum === 4){
                        colNum = 1
                    }
                }
            }
        }
    } catch(err) {
        const error = err.message
        const html = createAlert(error);
        const main = document.querySelector("#main")
        main.innerHTML = html
    }
});
